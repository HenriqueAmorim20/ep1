#ifndef ARQUIVOS_HPP
#define ARQUIVOS_HPP

#include <fstream>
#include <string>
#include "mapas.hpp"

using namespace std;

class Arquivo : public Mapa{
public:
  int n,a;
  fstream arquivo, arquivo2;
  string t, o;
  int i, j;
  Arquivo();
  ~Arquivo();
  void chama_mapa();
  void recebe_dados();
  void passa_dados();
  void reseta_temp();

};







#endif
