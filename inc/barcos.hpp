#ifndef BARCOS_HPP
#define BARCOS_HPP

#include <string>
#include "mapas.hpp"


using namespace std;

class Barcos {
public:

  int coordI, coordJ;
  string tipo;
  string orientacao;

};

class Porta_avioes : public Barcos, public Mapa{
public:
  Porta_avioes();
  Porta_avioes(int coordI, int coordJ, string orientacao);
  ~Porta_avioes();
  void habilidade();

};


class Submarino : public Barcos, public Mapa {
public:
  Submarino(int coordI, int coordJ, string orientacao);
  ~Submarino();
  void habilidade();

};

class Canoa : public Barcos, public Mapa {
public:
  Canoa(int coordI, int coordJ);
  ~Canoa();

};


#endif
