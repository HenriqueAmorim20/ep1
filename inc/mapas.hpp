#ifndef MAPAS_HPP
#define MAPAS_HPP
#define MAX 13
#include <string>

using namespace std;


class Mapa{
public:
  int i, j;
  string mapa[MAX][MAX];
  Mapa();
  ~Mapa();
  void imprime_mapa();
  void posiciona_barcos(int coordI, int coordJ,string tipo, string orientacao);

};


#endif
